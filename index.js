var express = require('express');

var app = express();

app.set('port',3008);



app.use('/', function (req, res) {
    res.send('This is an Express application');
})

app.listen(app.get('port'));
console.log('Server running at http://localhost:'+app.get('port'));

module.exports = app;