// Invoke 'strict' JavaScript mode
'use strict';

// Load the Mongoose module and Schema object
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    firstName : String,
    lastName: String,
    email: {
        type:String,
        index:true,
        match:/.+\@.+\..+/
    },
    username : {
        type: String,
        trim : true,
        unique:true,
        required : true
    },
    password : {
        type:String,
        validate:[
            function (password) {
                return password.length >= 6;
            },
            'Password should longer than 6 characters'
        ]
    },
    created : {
        type : Date,
        default : Date.now
    },
    website : {
        type : String,
        get : function (url){
            if(!url){
                return url;
            }else{
                if(url.indexOf('http://') !==0 && url.indexOf('https://') !==0){
                    url = 'http://' +url;
                }
                return url;
            }
        }
    }
});

UserSchema.statics.findOneByUsername = function (username, callback) {
    this.findOne({username:new RegExp(username, 'i')},callback);
};
UserSchema.methods.authenticate = function (password) {
    return this.password = password;
};

UserSchema.virtual('fullName').get(function () {
    return this.firstName + ' ' + this.lastName;
});

UserSchema.set('toJSON',{getters:true, virtuals:true});
mongoose.model('User',UserSchema);

//PostSchema

var PostSchema = new Schema({
    title:{
        type: String,
        required : true
    },
    content : {
        type:String,
        required:true
    },
    author : {
        type:Schema.ObjectId,
        ref:'User'
    }
});

mongoose.model('Post',PostSchema);