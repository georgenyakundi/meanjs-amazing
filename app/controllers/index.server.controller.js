exports.render = function (req, res) {
    // res.send('Hello Express from index.server.controller.js');
    if(req.session.lastVisit){
        console.log(req.session.lastVisit);
    }
    req.session.lastVisit = new Date();
    res.render('index',{
        title:'George of the Jungle'
    })
};