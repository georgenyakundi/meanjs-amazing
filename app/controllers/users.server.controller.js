// Invoke 'strict' JavaScript mode
'use strict';

// Load the 'User' Mongoose model
//var User = require('../models/user.server.model').model('User');
var User = require('mongoose').model('User');

// Create a new 'create' controller method
exports.create = function(req, res, next) {
    // Create a new instance of the 'User' Mongoose model
    var user = new User(req.body);

    // Use the 'User' instance's 'save' method to save a new user document
    user.save(function(err) {
        if (err) {
            // Call the next middleware with an error message
            return next(err);
        } else {
            // Use the 'response' object to send a JSON response
            res.json(user);
        }
    });
};

exports.list = function (req, res, next){
    User.find({},function (err, users) {
        if(err){
            return next(err);
        }else {
            res.json(users);
        }
    });
};

exports.read = function (req, res) {
  res.json(req.user);
};

exports.userById = function (req, res, next, id ) {
    User.findOne({
        _id:id,
    },function (err, user) {
        if(err){
            return next(err);
        }else{
            req.user = user;
            next();
        }
    });
};

exports.update = function (req, res, next) {
    User.findByIdAndUpdate(req.user.id, req.body, function (err,user) {
        if(err){
            return next(err);
        }else{
            res.json(user);
        }
    });
};

exports.delete = function (req, res, next) {
    req.user.remove(function (err) {
        if(err){
            return next(err);
        }else{
            res.json(req.user);
        }
    });
};